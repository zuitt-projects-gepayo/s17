console.log("hello World");


let grades = [98.5, 94.3, 89.2, 90.1];
let marvelHeroes = ["Iron Man", "Captain America", "Thor", "Hulk", "Black Widow", "Hawkeye","Shang chi", "SpriderMan"];


//Array and Indexes
    // [] = array Literals


    console.log(grades[2]);//89.2
    console.log(marvelHeroes[6]);




//Getting length or the Array

    //arrays have access to 
        //".length" property - to get the number elements present in an array; 
    
   console.log(marvelHeroes.length); //8

   if(marvelHeroes.length > 5 ){
       console.log(`We have too many heroes, Please contact Thanos`)
   };


   //Accessing the last element of an array
    // since the first element of an array starts with 0, subtracting 1 tothe length of an array will offset the value by one allowing us to get the last element.
   let lastElement = marvelHeroes.length-1;
   console.log(marvelHeroes[lastElement]);//SpiderMan


   //Array Methods
        //Mutator Methods
            //this are functions that "MUTATE" or change and array after they're created.

        let fruits = ["Appple", "Blueberry", "Orange", "Grapes"];

        //push
            //adds an element in the end of an array and returns the array's length.
        
        console.log(fruits);
        fruits.push("Mango");
        console.log(fruits);

        //adding muptiple element
        fruits.push("Guava","Kiwi");
        console.log(fruits);

        //pop()
            // Removes the last element in an array and returns the removed element
        
        let removedFruit = fruits.pop();
        console.log(fruits);
        console.log(removedFruit);

        fruits.pop()
        console.log(fruits);
       
        //unshift 
            //adds an element/s at the beginning of an array
        
        fruits.unshift("Guava");
        console.log(fruits);
        
        fruits.unshift("Kiwi", "Lime");
        console.log(fruits);

        //shift()
            //removing an element/s at the beginning of an array;
        
        fruits.shift();
        console.log(fruits);

        //splice()
            // simultaneously removes elements from a specified index number and adds elements

            /*
                SYNTAX
                    arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
            */
        
        fruits.splice(1, 2, "Cherry", "WaterMelon");
        console.log(fruits);


        fruits.splice(3);
        console.log(fruits);

    

        //adding in the middle of an array
        fruits.splice(2,0,"Cherry","Buko");
        console.log(fruits);
       
         //removing in the middle
         fruits.splice(1,1);
         console.log(fruits);

        //Sort()
            //rearranges the array elements in alphanumeric order.
        fruits.sort();
        console.log(fruits); 
        

        //reverse()
            //reverses the order of the elements

        fruits.reverse();
        console.log(fruits);

    
    //Non - Mutator Methods
        //these function do not modify or change an array

        let countries = ["US", "PH", "CAN", "SG", "TH", "PH","FR", "DE"]

        //indexOf()
            //returns the index number of the first matching element found in an array

            // SYNTAX:
                //arrayName.indexOf(searchValue)

        console.log(countries.indexOf("PH"));//1
        
        let indexoOfSG = countries.indexOf("SG")
        console.log(indexoOfSG);//3


        let invalidCountry = countries.indexOf("SK");
        console.log(invalidCountry); // -1 meaning cannot be found

        let invalidCountry2 = countries.indexOf("JP");
        console.log(invalidCountry2); // -1

        console.log(countries);

        //slice()
            //slice elements from an array and returns a new array
            /*
                Syntax:
                arrayName.slice(startingIndex);
                arrayName.slice(startingIndex, endingIndex);
            */
        // slice off elements from a specified index to the last element

        let sliceArrayA = countries.slice(2)
        console.log(sliceArrayA); //['CAN', 'SG', 'TH', 'PH', 'FR', 'DE']

        let sliceArrayB =countries.slice(2,4)
        console.log(sliceArrayB) // ['CAN', 'SG']

        //slice off elements starting from the last element of array
        let sliceArrayC = countries.slice(-3);
        console.log(sliceArrayC); // ['PH', 'FR', 'DE']
       

        //toString()
            //returns an array as string separated by commas
            //SYNTAX: 
                //arrayName.toString()

        let stringArray = countries.toString();
        console.log(stringArray);

        let sentence = ["I", "Like","JavaScript", ".", "Its", "Fun", "!"];
        let sentenceString = sentence.toString();
        console.log(sentenceString);

        //concat()
            //combines two arrays and returns a combined result
            //SYNTAX: arrayA.concat(arrayB);

        let tasksArrayA = ["dring HTML", "Eat javaScript"];
        let tasksArrayB = ["inhale CSS", "breathe SASS"];
        let tasksArrayC = ["get git", "be node"];

        let tasks = tasksArrayA.concat(tasksArrayB);
        console.log(tasks);

        let alltasks = tasksArrayA.concat(tasksArrayB,tasksArrayC);
        console.log(alltasks);

        let combinedTasks = tasksArrayA.concat("smell express", "throw react");
        console.log(combinedTasks);

        //join()
            //returns an array as a string separated by a specified separator string

        let members = ["Rose","Lisa", "Jisoo","Jennie"];
        let joinedMembers1 = members.join();
        console.log(joinedMembers1);// Rose,Lisa,Jisoo,Jennie

        let joinedMembers2 = members.join('');
        console.log(joinedMembers2); //RoseLisaJisooJennie

        let joinedMembers3 = members.join(" ");
        console.log(joinedMembers3);// Rose Lisa Jisoo Jennie

        let joinedMembers4 = members.join("/");
        console.log(joinedMembers4); // Rose/Lisa/Jisoo/Jennie


        //Iteration Methods
            // they are loops designed to perform repetitive tasks on arrays
            // usefull for manipulating array data resulting in complex tasks
            // 

        //forEach()
        let filteredTasks = [];
        alltasks.forEach(function(task) {
            console.log(task)

            if(task.length>10){
                filteredTasks.push(task);
            }
        });
        console.log(filteredTasks);

        //.map()
            //iterates on each lement and returns new array with different values
            //unlike forEach method, map method requires the use of return keyword in order to create another array with the performed operation
        
        let numbers = [1, 2, 3, 4, 5];

        let numberMap = numbers.map(function(number){
            return number * number;
        });
        console.log(numberMap);


        //every()
            //checks if all elements meet a certain condition
            //returns a true value if all elements meet the condition and flase if otherwise
        
        let allValid = numbers.every(function(num){
            return(num < 3);
        });
        console.log(allValid);//false

    
        //some()
            //checks at least on element in the array that meets the condition
            //returns a true value if at least 1 elements meet the condition and false if otherwise.

        let someValid = numbers.some(function(num){
            return(num < 2 );
        });
        console.log(someValid);//true

        //filter()
            //returns a new array that contains the elements that meets a certain condition.
            // if  there is no elements found, it will return an empty array.

        let filterValid = numbers.filter(function(num){
            return (num < 3 );
        });
        console.log(filterValid);//[1, 2]
        
        let colors = ["red", "green", "black", "orange", "yellow"];
        let values = ["red","black", "yellow"];

        colors= colors.filter(color => values.indexOf(color)===-1);
        console.log(colors);

        /*
        colors =colors.filter(function(item){
            return(values.indexOf(item)===-1)
        });
        */

        //includes()
            //returns a boolean value true if it finds a matching item in the array
            // includes us case-sensitive

        let products = ["Mouse", "Keyboard" , "Laptop", "Monitor"];

        let filterProducts = products.filter(function(product){
            return product.toLowerCase().includes("a");
        });

        console.log(filterProducts);


        let chessboard =[   ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
                            ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
                            ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
                            ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
                            ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
                            ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
                            ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
                            ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"]
                        ];

        console.log(chessboard[1][4]);






